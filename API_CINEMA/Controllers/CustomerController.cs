﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_CINEMA.Database;
using API_CINEMA.Models.Customer;

namespace API_CINEMA.Controllers
{
    public class CustomerController : ApiController
    {
        PTUDHTCinemaEntities connect = new PTUDHTCinemaEntities();

        public static string GenPass(string pass)
        {
            return BCrypt.Net.BCrypt.HashPassword(pass, 10);
        }

        public static bool CheckPass(string inputPass, string dbPass)
        {
            try
            {
                return BCrypt.Net.BCrypt.Verify(inputPass, dbPass);
            }
            catch
            {
                return false;
            }
        }

        [Route("api/createCustomer")]
        [HttpPost]
        public IHttpActionResult CreateCustomer([FromBody] CreateModel payload )
        {
            try
            {
                var query = (from c in connect.Customers
                             where payload.phone == c.phone
                             select c).ToList();
                if (query.Count() >= 1) return this.Content(HttpStatusCode.OK, new { status = 1, message = "Số điện thoại đã được đăng kí trên hệ thống", data = new { } });

                Customer cus = new Customer();
                cus.phone = payload.phone;
                cus.password = GenPass(payload.password);
                cus.name = payload.name;
                cus.role = 0;
                cus.isActive = 1;
                cus.image = payload.image;

                connect.Customers.Add(cus);
                connect.SaveChanges();

                return this.Content(HttpStatusCode.OK, new { status = 1, message = "Đăng kí tài khoản thành công", data = new { phone = payload.phone, password = payload.password } });
            }
            catch (Exception)
            {
                return this.Content(HttpStatusCode.BadRequest, new { status = 0, message = "Có lỗi xảy!", data = new { } });
            }
        }

        [Route("api/login")]
        [HttpPut]
        public IHttpActionResult Login([FromBody] LoginModel payload)
        {
            try
            {
                DetailModel customer = new DetailModel();
                var query = (from c in connect.Customers
                             where c.phone == payload.phone && c.isActive.Value == 1
                             select c).FirstOrDefault();
                if (query == null) return this.Content(HttpStatusCode.OK, new { status = 1, message = "Số điện thoại sai", data = new { } });
                if (CheckPass(payload.password, query.password))
                {
                    customer.id = query.id;
                    customer.phone = query.phone;
                    customer.name = query.name;
                    customer.image = query.image;
                    customer.role = (int)query.role;
                    customer.isActive = (int)query.isActive;
                    return this.Content(HttpStatusCode.OK, new { status = 1, message = "Thành công", data = customer });
                }
                else
                {
                    return this.Content(HttpStatusCode.OK, new { status = 1, message = "Mật khẩu sai", data = new { } });
                }
            }
            catch (Exception)
            {
                return this.Content(HttpStatusCode.BadRequest, new { status = 0, message = "Có lỗi xảy!", data = new { } });
            }
        }
    }
}
