﻿using API_CINEMA.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_CINEMA.Models.Ticket;

namespace API_CINEMA.Controllers
{
    public class TicketController : ApiController
    {
        PTUDHTCinemaEntities connect = new PTUDHTCinemaEntities();

        //lấy RoomId và FilmId, dateIn(giờ chiếu) để lấy list vé
        [Route("api/ticket")]
        [HttpGet]
        public IHttpActionResult GetTicket(int filmId, int roomId, string dateIn)
        {
            try
            {
                List<DetailModel> list = new List<DetailModel>();

                DateTime timeIn = DateTime.ParseExact(dateIn, "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);

                list = (from t in connect.Tickets
                        where (t.filmId == filmId && t.isActive.Value == 1 && t.roomId == roomId && t.dateIn == timeIn)
                        select new DetailModel
                        {
                            id = t.id,
                            filmId = t.filmId,
                            roomId = t.roomId,
                            customerId = t.customerId != null ? (int)t.customerId : 0,
                            name = t.name,
                            type = t.type,
                            price = t.price,
                            seat = t.seat,
                            dateIn = (DateTime)t.dateIn,
                            dateOut = (DateTime)t.dateOut,
                        }).ToList();

                return this.Content(HttpStatusCode.OK, new { status = 1, message = "Thành công", data = list });
            }
            catch (Exception)
            {
                return this.Content(HttpStatusCode.BadRequest, new { status = 0, message = "Có lỗi xảy!", data = new { } });
            }
        }

        public class ItemListCheck
        {
            public int id { get; set; }
            public int filmId { get; set; }
            public DateTime dateIn { get; set; }
            public DateTime dateOut { get; set; }
        }
        //tạo vé
        [Route("api/createTicket")]
        [HttpPost]
        public IHttpActionResult CreateTicket([FromBody] CreateModel payload)
        {
            try
            {
                var listCheck = (from t in connect.Tickets
                                 where (t.roomId == payload.roomId && t.isActive.Value == 1)
                                 select t).ToList();

                if (listCheck.Count > 0)
                {
                    foreach (var item in listCheck)
                    {
                        if (item.filmId != payload.filmId)
                        {
                            var timeDB = (DateTime)item.dateOut;
                            var timePayload = DateTime.Parse(payload.dateIn);
                            
                            if(timeDB.Year == timePayload.Year && timeDB.Month == timePayload.Month && timeDB.Day == timePayload.Day)
                            {
                                if (timeDB.Hour > timePayload.Hour)
                                {
                                    return this.Content(HttpStatusCode.OK, new { status = 1, message = "Đã có phim đăng kí chiếu trong thời gian này", data = new { } });
                                }

                                if (timeDB.Hour == timePayload.Hour)
                                {
                                    if (timeDB.Minute > timePayload.Minute)
                                    {
                                        return this.Content(HttpStatusCode.OK, new { status = 1, message = "Đã có phim đăng kí chiếu trong thời gian này", data = new { } });
                                    }
                                }
                            }
                        }

                    }
                }

                for (int i = 0; i < payload.count; i++)
                {
                    Ticket ticket = new Ticket();

                    ticket.filmId = payload.filmId;
                    ticket.roomId = payload.roomId;
                    ticket.name = payload.name;
                    ticket.type = 1;
                    ticket.price = payload.price;
                    ticket.seat = $"{i+1}";
                    ticket.isActive = 1;
                    ticket.dateIn = DateTime.ParseExact(payload.dateIn, "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
                    ticket.dateOut = DateTime.ParseExact(payload.dateOut, "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);

                    connect.Tickets.Add(ticket);
                    connect.SaveChanges();
                }

                return this.Content(HttpStatusCode.OK, new { status = 1, message = "Thêm thành công vé phim " + payload.name, data = new { } });
            }
            catch (Exception)
            {
                return this.Content(HttpStatusCode.BadRequest, new { status = 0, message = "Có lỗi xảy!", data = new { } });
            }
        }

        public class Pick
        {
            public int ticketId { get; set; }

            public int customerId { get; set; }
        }
        //đặt vé
        [Route("api/pickTicket")]
        [HttpPut]
        public IHttpActionResult PickTiket([FromBody] Pick payload)
        {
            try
            {
                Ticket ticket = connect.Tickets.Find(payload.ticketId);
                ticket.customerId = payload.customerId;
                connect.SaveChanges();
                return this.Content(HttpStatusCode.OK, new { status = 1, message = "Thành công", data = new { } });
            }
            catch (Exception)
            {
                return this.Content(HttpStatusCode.BadRequest, new { status = 0, message = "Có lỗi xảy!", data = new { } });
            }
        }

        //lấy danh sách vé cá nhân
        [Route("api/getTicketOfCustomer")]
        [HttpGet]
        public IHttpActionResult GetTicketOfCustomer(int customerId)
        {
            try
            {
                List<DetailModel> list = new List<DetailModel>();

                list = (
                    from t in connect.Tickets
                    where t.customerId == customerId
                    select new DetailModel
                    {
                        id = t.id,
                        filmId = t.filmId,
                        roomId = t.roomId,
                        customerId = (int)t.customerId,
                        name = t.name,
                        type = t.type,
                        price = t.price,
                        seat = t.seat,
                        dateIn = (DateTime)t.dateIn,
                        dateOut = (DateTime)t.dateOut,
                    }).ToList();
                
                return this.Content(HttpStatusCode.OK, new { status = 1, message = "Thành công", data = list });
            }
            catch (Exception)
            {
                return this.Content(HttpStatusCode.BadRequest, new { status = 0, message = "Có lỗi xảy!", data = new { } });
            }
        }
    }
}
