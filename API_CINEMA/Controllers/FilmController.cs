﻿using API_CINEMA.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API_CINEMA.Models.Film;

namespace API_CINEMA.Controllers
{
    public class FilmController : ApiController
    {
        PTUDHTCinemaEntities connect = new PTUDHTCinemaEntities();

        [Route("api/getFilm")]
        [HttpGet]
        public IHttpActionResult GetFilm()
        {
            try
            {
                List<DetailModel> data = new List<DetailModel>();

                data = (from f in connect.Films
                             where f.isActive.Value == 1
                             select new DetailModel
                             {
                                 id = f.id,
                                 name = f.name,
                                 type = f.type,
                                 date = f.date,
                                 image = f.image,
                                 description = f.description,
                             }).ToList();

                return this.Content(HttpStatusCode.OK, new { status = 1, message = "Thành công", data = data });
            }
            catch (Exception)
            {
                return this.Content(HttpStatusCode.BadRequest, new { status = 0, message = "Có lỗi xảy!", data = new { } });
            }
        }

        [Route("api/getSchedle")]
        [HttpGet]
        public IHttpActionResult GetSchedle(int RoomId)
        {
            try
            {
                IList<SchedleModel> schedle = new List<SchedleModel>();

                schedle = (from f in connect.Films
                             join t in connect.Tickets on f.id equals t.filmId
                             join r in connect.Rooms on t.roomId equals r.id
                             where (f.isActive.Value == 1 && r.id == RoomId)
                             select new SchedleModel
                             {
                                 id = f.id,
                                 name = f.name,
                                 type = f.type,
                                 date = f.date,
                                 image = f.image,
                                 description = f.description,
                                 dateIn = (DateTime)t.dateIn,
                                 dateOut = (DateTime)t.dateOut,
                                 roomId = r.id,
                             }).ToList();

                //Phương thức Distinct trong LinQ
                var distinctSchedle = schedle.Distinct(new SchedleModelComparer());

                return this.Content(HttpStatusCode.OK, new { status = 1, message = "Thành công", data = distinctSchedle });
            }
            catch (Exception)
            {
                return this.Content(HttpStatusCode.BadRequest, new { status = 0, message = "Có lỗi xảy!", data = new { } });
            }
        }

        [Route("api/createFilm")]
        [HttpPost]
        public IHttpActionResult CreateFilm([FromBody] CreateModel payload)
        {
            try
            {
                Film film = new Film();

                film.name = payload.name;
                film.type = payload.type;
                film.date = payload.date;
                film.image = payload.image;
                film.description = payload.description;
                film.isActive = 1;
                connect.Films.Add(film);
                connect.SaveChanges();

                return this.Content(HttpStatusCode.OK, new { status = 1, message = "Thêm thành công phim " + payload.name + " vào danh sách", data = new {  } });
            }
            catch (Exception)
            {
                return this.Content(HttpStatusCode.BadRequest, new { status = 0, message = "Có lỗi xảy!", data = new { } });
            }
        }

        [Route("api/deleteFilm")]
        [HttpDelete]
        public IHttpActionResult DeleteFilm(int id)
        {
            try
            {
                var film = connect.Films.Find(id);
                film.isActive = 0;
                connect.SaveChanges();
                return this.Content(HttpStatusCode.OK, new { status = 1, message = "Xoá thành công phim " + film.name, data = new { } });
            }
            catch (Exception)
            {
                return this.Content(HttpStatusCode.BadRequest, new { status = 0, message = "Có lỗi xảy!", data = new { } });
            }
        }
    }
}
