﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_CINEMA.Models.Customer
{
    public class CreateModel
    {
        public string phone { get; set; }

        public string password { get; set; }

        public string name { get; set; }

        public string image { get; set; }
    }
}