﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_CINEMA.Models.Customer
{
    public class DetailModel
    {
        public int id { get; set; }
        public string phone { get; set; }

        public string password { get; set; }

        public string name { get; set; }

        public string image { get; set; }

        public int role { get; set; }

        public int isActive { get; set; }
    }
}