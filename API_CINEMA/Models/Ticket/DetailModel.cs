﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_CINEMA.Models.Ticket
{
    public class DetailModel
    {
        public int id { get; set; }
        public int filmId { get; set; }
        public int roomId { get; set; }
        public int customerId { get; set; }
        public string name { get; set; }
        public int type { get; set; }
        public int price { get; set; }
        public string seat { get; set; }
        public DateTime dateIn { get; set; }
        public DateTime dateOut { get; set; }
        public int isActive { get; set; }
        //public string roomName { get; set; }
    }
}