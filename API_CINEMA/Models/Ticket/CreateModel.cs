﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_CINEMA.Models.Ticket
{
    public class CreateModel
    {
        public int filmId { get; set; }
        public int roomId { get; set; }
        public string name { get; set; }
        //public int type { get; set; }
        public int price { get; set; }
        public string dateIn { get; set; }
        public string dateOut { get; set; }
        public int count { get; set; }
    }
}