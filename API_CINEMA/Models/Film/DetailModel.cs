﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_CINEMA.Models.Film
{
    public class DetailModel
    {
        public int id { get; set; }

        public string name { get; set; }

        public string type { get; set; }

        public string date { get; set; }

        public string image { get; set; }

        public string description { get; set; }
    }
}