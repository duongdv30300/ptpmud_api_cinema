﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API_CINEMA.Models.Film
{
    public class SchedleModel
    {
        public int id { get; set; }

        public string name { get; set; }

        public string type { get; set; }

        public string date { get; set; }

        public string image { get; set; }

        public string description { get; set; }

        public DateTime dateIn { get; set; }

        public DateTime dateOut { get; set; }

        public int roomId { get; set; }

    }

    //Phương thức Distinct trong LinQ
    //Phương thức mở rộng Distinct trả về một tập hợp các phần tử duy nhất từ danh sách đã cho
    class SchedleModelComparer : IEqualityComparer<SchedleModel>
    {
        public bool Equals(SchedleModel x, SchedleModel y)
        {
            return x.id == y.id && x.dateIn == y.dateIn && x.dateOut == y.dateOut;
        }

        public int GetHashCode(SchedleModel obj)
        {
            return obj.id.GetHashCode();
        }
    }
}